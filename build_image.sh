#!/usr/bin/env bash
set -x

function mkd {
  dir="$1"
  if ! [ -d "${dir}" ]; then
    mkdir -p "${dir}"
  fi
}

function build_date {
  echo $(date "+%Y%m%d%H%M")
}

function prepare_files {
  target="${1}"
  cp ~/.screenrc ~/.bashrc "${target}"
}

function build_image {
  # reference for docker build cmd:  https://docs.docker.com/engine/reference/commandline/build/
  # dockerfile reference: https://docs.docker.com/engine/reference/builder/#usage
  # best practises for build: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

  # usefull when you have differenet dockerfile name, for example for development and release build
  dfl='Dockerfile' 

  # where is correct Dockerfile located? Everytime create folder for build. 
  # All files in "context" folder are available for build - if you upload big file in daemon and you don't need it, you slowdown yourself
  build_cnt='./docker_build_wrong/' 

  # based on documentation using --build_arg parameter you should be able to pass variable in dockerfile - it never worked for me
  #DONT ENABLE build_args="--build-arg WORKDIR=/opt/" #PACKAGES=htop 

  # name of image
  image_name='test_img'

  # which version of image you build? in our case based on actual date
  # common practise is tag image twice
  #  - actual_version
  #  - latest - using this tag user everytime can pull, but he doesn't know which version he has :(
  tag=$(build_date)

  # if you don't have strange troubles don't use - you force to rebuild everything everytime
  no_cache='--no-cache'
  
  mkd "${build_cnt}"
  prepare_files "${build_cnt}"

  echo ""
  
  docker build \
    --file ${build_cnt}${dfl} \
    --tag ${image_name}:${tag} \
    ${no_cache} \
    ${build_cnt}
}

build_image $@

#  build_cnt='./docker_build/' 

